package com.example.sixthfriday.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ItemViewModel: ViewModel() {

    private val repository: ItemRepository = ItemRepository(DataBase.db.itemDao())

    val readAllData: LiveData<List<Item>> = repository.readAllData

    fun addItem(item: Item){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addItem(item)
        }
    }
}