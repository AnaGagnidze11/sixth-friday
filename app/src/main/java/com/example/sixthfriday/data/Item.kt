package com.example.sixthfriday.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Items")
data class Item(@PrimaryKey(autoGenerate = true)
                val id: Int,
                val title: String,
                val description: String,
                val picture: Int): Parcelable{
    constructor(title: String, description: String, picture: Int): this(0, title, description, picture)
}
