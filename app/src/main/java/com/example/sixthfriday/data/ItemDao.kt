package com.example.sixthfriday.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ItemDao {
    @Insert
    suspend fun addItem(item: Item)

    @Query("SELECT * FROM items ORDER BY id ASC")
    fun readAllData(): LiveData<List<Item>>
}