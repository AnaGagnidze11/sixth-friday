package com.example.sixthfriday.data

import androidx.room.Room
import com.example.sixthfriday.App

object DataBase {
    val db: ItemsDatabase by lazy {
        Room.databaseBuilder(App.context!!, ItemsDatabase::class.java, "items").build()
    }
}