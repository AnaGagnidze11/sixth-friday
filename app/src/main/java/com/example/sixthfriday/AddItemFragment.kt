package com.example.sixthfriday

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sixthfriday.data.Item
import com.example.sixthfriday.data.ItemViewModel
import com.example.sixthfriday.databinding.FragmentAddItemBinding


class AddItemFragment : Fragment() {

    private lateinit var binding: FragmentAddItemBinding

    private val viewModel: ItemViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddItemBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        binding.addUserButton.setOnClickListener {
            insertItemInDB()
        }
    }

    private fun insertItemInDB(){
        val title = binding.titleEdtTxt.text.toString()
        val desc = binding.descEdtTxt.text.toString()

        if (title.isNotEmpty() && desc.isNotEmpty()){
            val item = Item(title, desc, R.mipmap.ic_launcher)
            viewModel.addItem(item)

            Toast.makeText(requireContext(), "Item has been added.", Toast.LENGTH_SHORT).show()

            findNavController().navigate(R.id.action_addItemFragment_to_itmesFragment)
        }else{
            Toast.makeText(requireContext(), "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

}