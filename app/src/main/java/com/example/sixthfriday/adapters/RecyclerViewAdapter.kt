package com.example.sixthfriday.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.ListFragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sixthfriday.data.Item
import com.example.sixthfriday.databinding.AddedItemLayoutBinding

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    private var itemList = emptyList<Item>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(AddedItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount()=itemList.size

    inner class ItemViewHolder(private val binding: AddedItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var item: Item
        fun bind(){
            item = itemList[adapterPosition]
            binding.returnedTitleTxt.text = item.title
            binding.returnedDescTxt.text = item.description
            binding.returnedPicImg.setImageResource(item.picture)

            binding.wholeItem.setOnClickListener {
            }

        }
    }


    fun setData(items: List<Item>){
        this.itemList = items
        notifyDataSetChanged()
    }
}