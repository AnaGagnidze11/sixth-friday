package com.example.sixthfriday

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sixthfriday.adapters.RecyclerViewAdapter
import com.example.sixthfriday.data.ItemViewModel
import com.example.sixthfriday.databinding.FragmentItmesBinding

class ItemsFragment : Fragment() {

    private lateinit var binding: FragmentItmesBinding

    private val viewModel: ItemViewModel by viewModels()

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentItmesBinding.inflate(inflater, container, false)
        openAddFragment()
        initRecyclerView()
        observeData()
        return binding.root
    }

    private fun openAddFragment(){
        binding.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_itmesFragment_to_addItemFragment)
        }
    }

    private fun initRecyclerView(){
        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    private fun observeData(){
        viewModel.readAllData.observe(viewLifecycleOwner,{
            adapter.setData(it)
        })
    }

}